package ch.kramerit.kassenbuch.controller;


import ch.kramerit.kassenbuch.dto.BankAccountDto;
import io.restassured.RestAssured;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static io.restassured.RestAssured.given;
import static io.restassured.http.ContentType.JSON;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

@SpringBootTest(webEnvironment = RANDOM_PORT)
public class BankAccountControllerIT {

    private final List<BankAccountDto> bankAccountDtos = new ArrayList<>();

    @LocalServerPort
    private int port;

    @BeforeEach
    public void init() {
        RestAssured.port = port;

        BankAccountDto bankAccountDto = new BankAccountDto();
        bankAccountDto.setName("Lohnkonto");
        bankAccountDto.setBank("Berner Kantonalbank");
        bankAccountDto.setIban("CH60 0079 0042 4847 9238 2");
        bankAccountDto.setCurrency("CHF");
        bankAccountDto.setOpenedDate(LocalDate.of(2010, 1, 1));

        bankAccountDtos.add(bankAccountDto);

        BankAccountDto bankAccountDto2 = new BankAccountDto();
        bankAccountDto2.setName("Lohnkonto");
        bankAccountDto2.setBank("Berner Kantonalbank");
        bankAccountDto2.setIban("CH60 0079 0042 4847 9238 2");
        bankAccountDto2.setCurrency("EUR");
        bankAccountDto2.setOpenedDate(LocalDate.of(2019, 1, 1));

        bankAccountDtos.add(bankAccountDto2);

        BankAccountDto bankAccountDto3 = new BankAccountDto();
        bankAccountDto3.setName("Lohnkonto");
        bankAccountDto3.setBank("Berner Kantonalbank");
        bankAccountDto3.setIban("CH60 0079 0042 4847 9238 2");
        bankAccountDto3.setCurrency("E");
        bankAccountDto3.setOpenedDate(LocalDate.of(2019, 1, 1));

        bankAccountDtos.add(bankAccountDto3);
    }

    @Test
    public void test() {
        BankAccountDto bankAccountDto = bankAccountDtos.get(0);

        Integer id = given().contentType(JSON).body(bankAccountDto)
                .post("/api/account").then()
                .statusCode(201)
                .extract().path("id");

        given().accept(JSON)
                .get("/api/account/" + id).then()
                .statusCode(200);

        given().accept(JSON)
                .get("/api/account/99").then()
                .statusCode(404);

        given().contentType(JSON).body(bankAccountDtos.get(2))
                .post("/api/account").then()
                .statusCode(400);
    }
}
