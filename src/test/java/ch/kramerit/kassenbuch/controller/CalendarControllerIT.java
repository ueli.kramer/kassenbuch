package ch.kramerit.kassenbuch.controller;


import io.restassured.RestAssured;
import io.restassured.response.ExtractableResponse;
import io.restassured.response.Response;
import net.minidev.json.JSONObject;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.event.annotation.BeforeTestClass;

import static io.restassured.RestAssured.given;
import static io.restassured.http.ContentType.JSON;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

@SpringBootTest(webEnvironment = RANDOM_PORT)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class CalendarControllerIT {
    @LocalServerPort
    private int port;

    @BeforeTestClass
    public void init() {
        RestAssured.port = port;
    }

    @Test
    @Order(1)
    public void createScheduledEntry() {
        JSONObject requestParams = new JSONObject();
        requestParams.put("bank_account", 1);
        requestParams.put("title", "Test entry");
        requestParams.put("date", "2020-12-01");
        requestParams.put("amount", 10.20);

        ExtractableResponse<Response> response = given()
                .contentType(JSON)
                .body(requestParams.toJSONString())
                .post("/api/calendar")
                .then()
                .statusCode(201)
                .extract();

        assertEquals(1, (Integer) response.path("occurrence"));
        assertEquals("scheduled", response.path("type"));
        assertEquals(1, (Integer) response.path("bank_account"));
        assertEquals("Test entry", response.path("title"));
        assertEquals("2020-12-01", response.path("date"));
        assertNull(response.path("recurring_setup"));
        assertNull(response.path("parent_entry"));

        // invalid date
        requestParams = new JSONObject();
        requestParams.put("bank_account", 1);
        requestParams.put("title", "Test entry");
        requestParams.put("date", "2020-11-31");
        requestParams.put("amount", 10.20);

        given().contentType(JSON)
                .body(requestParams.toJSONString())
                .post("/api/calendar")
                .then()
                .statusCode(400);

        // invalid bank_account
        requestParams = new JSONObject();
        requestParams.put("bank_account", 2);
        requestParams.put("title", "Test entry");
        requestParams.put("date", "2020-12-01");
        requestParams.put("amount", 10.20);

        given().contentType(JSON)
                .body(requestParams.toJSONString())
                .post("/api/calendar")
                .then()
                .statusCode(400);

        // missing required field
        requestParams = new JSONObject();
        requestParams.put("bank_account", 1);
        requestParams.put("title", "");
        requestParams.put("date", "2020-12-01");
        requestParams.put("amount", 10.20);

        given().contentType(JSON)
                .body(requestParams.toJSONString())
                .post("/api/calendar")
                .then()
                .statusCode(400);


        // recurring
        JSONObject recurringSetup = new JSONObject();
        requestParams.put("title", "This is a test");
        recurringSetup.put("type", "DAILY");
        recurringSetup.put("separation_count", 1);
        recurringSetup.put("after_holiday", false);
        recurringSetup.put("number_of_occurrences", 2);

        requestParams.put("recurring_setup", recurringSetup);
        response = given().contentType(JSON)
                .body(requestParams.toJSONString())
                .post("/api/calendar")
                .then()
                .statusCode(201)
                .extract();
        assertEquals(2, (Integer) response.path("recurring_setup.number_of_occurrences"));
    }

    @Test
    @Order(2)
    public void getAllEventsInDateRange() {
        ExtractableResponse<Response> response = given()
                .contentType(JSON)
                .queryParam("from", "2020-12-01")
                .queryParam("to", "2020-12-31")
                .get("/api/calendar")
                .then()
                .statusCode(200)
                .extract();

        assertEquals(3, response.jsonPath().getList("$").size());


        response = given()
                .contentType(JSON)
                .queryParam("from", "2020-11-01")
                .queryParam("to", "2020-12-01")
                .get("/api/calendar")
                .then()
                .statusCode(200)
                .extract();

        assertEquals(2, response.jsonPath().getList("$").size());
    }

    @Test
    @Order(3)
    public void updateScheduledEntry() {
        // @todo: implement test
    }

    @Test
    @Order(4)
    public void bookScheduledEntry() {
        ExtractableResponse<Response> response = given()
                .contentType(JSON)
                .post("/api/calendar/1/book")
                .then()
                .statusCode(200)
                .extract();

        assertEquals("booked", response.path("type"));

        given()
                .contentType(JSON)
                .post("/api/calendar/1/book")
                .then()
                .statusCode(404);


        // test the booking of a recurring scheduled entry
        given()
                .contentType(JSON)
                .post("/api/calendar/2/book")
                .then()
                .statusCode(200);

        given()
                .contentType(JSON)
                .post("/api/calendar/2/book")
                .then()
                .statusCode(200);

        given()
                .contentType(JSON)
                .post("/api/calendar/2/book")
                .then()
                .statusCode(404);
    }
}
