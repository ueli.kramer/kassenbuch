package ch.kramerit.kassenbuch.util;

import de.jollyday.HolidayCalendar;
import de.jollyday.HolidayManager;
import de.jollyday.ManagerParameters;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDate;

import static ch.kramerit.kassenbuch.enumeration.RecurringTypeEnum.*;
import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
public class ScheduleUtilTest {
    public void before() {
        new ScheduleUtil(HolidayManager.getInstance(ManagerParameters.create(HolidayCalendar.SWITZERLAND)));
    }

    @Test
    public void isWeekday() {
        assertTrue(ScheduleUtil.isWeekday(LocalDate.of(2020, 12, 11)));
        assertFalse(ScheduleUtil.isWeekday(LocalDate.of(2020, 12, 12)));
        assertFalse(ScheduleUtil.isWeekday(LocalDate.of(2020, 12, 13)));
    }

    @Test
    public void isHoliday() {
        assertTrue(ScheduleUtil.isHoliday(LocalDate.of(2020, 12, 26)));
        assertFalse(ScheduleUtil.isHoliday(LocalDate.of(2020, 12, 12)));
    }

    @Test
    public void getClosestBusinessDayBeforeHoliday() {
        assertEquals(
                LocalDate.of(2020, 12, 24),
                ScheduleUtil.getClosestBusinessDay(LocalDate.of(2020, 12, 26), false)
        );
        assertEquals(
                LocalDate.of(2020, 12, 11),
                ScheduleUtil.getClosestBusinessDay(LocalDate.of(2020, 12, 12), false)
        );
        assertEquals(
                LocalDate.of(2020, 12, 11),
                ScheduleUtil.getClosestBusinessDay(LocalDate.of(2020, 12, 13), false)
        );
        assertEquals(
                LocalDate.of(2020, 12, 31),
                ScheduleUtil.getClosestBusinessDay(LocalDate.of(2021, 1, 2), false)
        );
    }

    @Test
    public void getClosestBusinessDayAfterHoliday() {
        assertEquals(
                LocalDate.of(2020, 12, 28),
                ScheduleUtil.getClosestBusinessDay(LocalDate.of(2020, 12, 26), true)
        );
        assertEquals(
                LocalDate.of(2020, 12, 14),
                ScheduleUtil.getClosestBusinessDay(LocalDate.of(2020, 12, 12), true)
        );
        assertEquals(
                LocalDate.of(2020, 12, 14),
                ScheduleUtil.getClosestBusinessDay(LocalDate.of(2020, 12, 13), true)
        );
        assertEquals(
                LocalDate.of(2021, 1, 4),
                ScheduleUtil.getClosestBusinessDay(LocalDate.of(2021, 1, 2), true)
        );
    }

    @Test
    public void addInterval() {
        LocalDate date = LocalDate.of(2020, 12, 13);
        // Daily
        assertEquals(
                LocalDate.of(2020, 12, 14),
                ScheduleUtil.addInterval(date, DAILY, 1, 2)
        );
        assertEquals(
                LocalDate.of(2020, 12, 15),
                ScheduleUtil.addInterval(date, DAILY, 2, 2)
        );
        assertEquals(
                LocalDate.of(2020, 12, 15),
                ScheduleUtil.addInterval(date, DAILY, 1, 3)
        );
        assertEquals(
                LocalDate.of(2020, 12, 16),
                ScheduleUtil.addInterval(date, DAILY, 3, 2)
        );
        // Weekly
        assertEquals(
                LocalDate.of(2020, 12, 20),
                ScheduleUtil.addInterval(date, WEEKLY, 1, 2)
        );
        assertEquals(
                LocalDate.of(2020, 12, 27),
                ScheduleUtil.addInterval(date, WEEKLY, 2, 2)
        );
        assertEquals(
                LocalDate.of(2021, 1, 3),
                ScheduleUtil.addInterval(date, WEEKLY, 3, 2)
        );
        // Monthly
        assertEquals(
                LocalDate.of(2021, 1, 13),
                ScheduleUtil.addInterval(date, MONTHLY, 1, 2)
        );
        assertEquals(
                LocalDate.of(2021, 2, 13),
                ScheduleUtil.addInterval(date, MONTHLY, 2, 2)
        );
        assertEquals(
                LocalDate.of(2021, 3, 13),
                ScheduleUtil.addInterval(date, MONTHLY, 3, 2)
        );
        // Yearly
        assertEquals(
                LocalDate.of(2021, 12, 13),
                ScheduleUtil.addInterval(date, YEARLY, 1, 2)
        );
        assertEquals(
                LocalDate.of(2022, 12, 13),
                ScheduleUtil.addInterval(date, YEARLY, 2, 2)
        );
        assertEquals(
                LocalDate.of(2023, 12, 13),
                ScheduleUtil.addInterval(date, YEARLY, 3, 2)
        );

        date = LocalDate.of(2020, 1, 31);
        assertEquals(
                LocalDate.of(2020, 2, 29),
                ScheduleUtil.addInterval(date, MONTHLY, 1, 2)
        );
        assertEquals(
                LocalDate.of(2020, 3, 31),
                ScheduleUtil.addInterval(date, MONTHLY, 2, 2)
        );
        assertEquals(
                LocalDate.of(2020, 4, 30),
                ScheduleUtil.addInterval(date, MONTHLY, 3, 2)
        );
    }
}
