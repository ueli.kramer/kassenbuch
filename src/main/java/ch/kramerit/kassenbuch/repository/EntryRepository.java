package ch.kramerit.kassenbuch.repository;

import ch.kramerit.kassenbuch.entity.BankAccount;
import ch.kramerit.kassenbuch.entity.Entry;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface EntryRepository extends JpaRepository<Entry, Long> {
    @Query("select e from Entry e where e.date >= :from AND e.date <= :to")
    List<Entry> findAllByFromAndToDate(@Param("from") LocalDate from, @Param("to") LocalDate to);

    @Query("select e from Entry e where e.bankAccount = :bankAccount and e.date <= :to")
    List<Entry> findAllByBankAccountAndToDate(@Param("bankAccount") BankAccount bankAccount, @Param("to") LocalDate to);

    List<Entry> findAllByBankAccount_IdAndDateLessThanEqual(Long bankAccountId, LocalDate dateTo);
}
