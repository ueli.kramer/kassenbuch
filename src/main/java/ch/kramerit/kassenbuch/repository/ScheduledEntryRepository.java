package ch.kramerit.kassenbuch.repository;

import ch.kramerit.kassenbuch.entity.BankAccount;
import ch.kramerit.kassenbuch.entity.ScheduledEntry;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface ScheduledEntryRepository extends JpaRepository<ScheduledEntry, Long> {
    @Query("select se from ScheduledEntry se where (se.date >= :from AND se.date <= :to) OR (se.recurringSetup IS NOT NULL AND se.date <= :to)")
    List<ScheduledEntry> findAllByFromAndToDate(@Param("from") LocalDate from, @Param("to") LocalDate to);

    @Query("select se from ScheduledEntry se where se.bankAccount = :bankAccount and (se.date <= :to OR (se.recurringSetup IS NOT NULL AND se.date <= :to))")
    List<ScheduledEntry> findAllByBankAccountAndToDate(@Param("bankAccount") BankAccount bankAccount, @Param("to") LocalDate to);
}
