package ch.kramerit.kassenbuch.controller;

import ch.kramerit.kassenbuch.dto.BankAccountBalanceDto;
import ch.kramerit.kassenbuch.dto.BankAccountDto;
import ch.kramerit.kassenbuch.entity.BankAccount;
import ch.kramerit.kassenbuch.entity.Entry;
import ch.kramerit.kassenbuch.entity.EntryInterface;
import ch.kramerit.kassenbuch.service.BankAccountService;
import ch.kramerit.kassenbuch.service.EntryService;
import org.modelmapper.ModelMapper;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.format.annotation.DateTimeFormat.ISO;

@RestController
@RequestMapping("/api/account")
public class BankAccountController {
    private final ModelMapper modelMapper;

    private final BankAccountService bankAccountService;

    private final EntryService entryService;

    public BankAccountController(final ModelMapper modelMapper, final BankAccountService bankAccountService, final EntryService entryService) {
        this.modelMapper = modelMapper;
        this.bankAccountService = bankAccountService;
        this.entryService = entryService;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public BankAccountDto create(@RequestBody @Valid final BankAccountDto bankAccountDto) {
        BankAccount bankAccount = convertToDao(bankAccountDto);
        return convertToDto(bankAccountService.create(bankAccount));
    }

    @GetMapping
    public List<BankAccountDto> list() {
        return bankAccountService.getAll().stream().map(this::convertToDto).collect(Collectors.toList());
    }

    @GetMapping("/{id}")
    public BankAccountDto findById(@PathVariable final long id) {
        return convertToDto(bankAccountService.get(id));
    }

    @GetMapping("/balances/{date}")
    public List<BankAccountBalanceDto> getBalanceOfAccount(@PathVariable @DateTimeFormat(iso = ISO.DATE) final LocalDate date) {
        List<BankAccount> bankAccounts = bankAccountService.getAll();
        List<BankAccountBalanceDto> bankAccountBalanceDtos = new ArrayList<>();
        for (BankAccount bankAccount : bankAccounts) {
            BankAccountDto bankAccountDto = convertToDto(bankAccount);
            BankAccountBalanceDto bankAccountBalanceDto = new BankAccountBalanceDto();
            bankAccountBalanceDto.setBankAccount(bankAccountDto);
            bankAccountBalanceDto.setCurrency(bankAccountDto.getCurrency());

            BigDecimal sum = entryService.getBookedEntriesByBankAccountAndDate(bankAccount, date)
                    .stream().map(Entry::getAmount)
                    .reduce(BigDecimal.ZERO, BigDecimal::add);
            bankAccountBalanceDto.setAmount(sum);
            bankAccountBalanceDtos.add(bankAccountBalanceDto);
        }

        return bankAccountBalanceDtos;
    }

    @GetMapping("/forecasts/{date}")
    public List<BankAccountBalanceDto> getForecastOfAccount(@PathVariable @DateTimeFormat(iso = ISO.DATE) final LocalDate date) {
        List<BankAccount> bankAccounts = bankAccountService.getAll();
        List<BankAccountBalanceDto> bankAccountBalanceDtos = new ArrayList<>();
        for (BankAccount bankAccount : bankAccounts) {
            BankAccountDto bankAccountDto = convertToDto(bankAccount);
            BankAccountBalanceDto bankAccountBalanceDto = new BankAccountBalanceDto();
            bankAccountBalanceDto.setBankAccount(bankAccountDto);
            bankAccountBalanceDto.setCurrency(bankAccountDto.getCurrency());

            BigDecimal sum = entryService.getAllEntriesByBankAccountAndDate(bankAccount, date)
                    .stream().map(EntryInterface::getAmount)
                    .reduce(BigDecimal.ZERO, BigDecimal::add);
            bankAccountBalanceDto.setAmount(sum);
            bankAccountBalanceDtos.add(bankAccountBalanceDto);
        }

        return bankAccountBalanceDtos;
    }

    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public BankAccountDto updateEntry(@PathVariable final long id, @RequestBody @Valid final BankAccountDto bankAccountDto) {
        BankAccount bankAccount = convertToDao(bankAccountDto);
        return convertToDto(bankAccountService.update(id, bankAccount));
    }

    private BankAccount convertToDao(final BankAccountDto bankAccountDto) {
        return modelMapper.map(bankAccountDto, BankAccount.class);
    }

    private BankAccountDto convertToDto(final BankAccount bankAccount) {
        return modelMapper.map(bankAccount, BankAccountDto.class);
    }
}
