package ch.kramerit.kassenbuch.controller;

import ch.kramerit.kassenbuch.dto.EntryDto;
import ch.kramerit.kassenbuch.dto.EntryDtoInterface;
import ch.kramerit.kassenbuch.dto.ScheduledEntryDto;
import ch.kramerit.kassenbuch.entity.Entry;
import ch.kramerit.kassenbuch.entity.EntryInterface;
import ch.kramerit.kassenbuch.entity.ScheduledEntry;
import ch.kramerit.kassenbuch.service.EntryService;
import org.modelmapper.ModelMapper;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.format.annotation.DateTimeFormat.ISO;

@RestController
@RequestMapping("/api/calendar")
public class CalendarController {

    private final ModelMapper modelMapper;
    private final EntryService entryService;

    public CalendarController(final ModelMapper modelMapper, final EntryService entryService) {
        this.modelMapper = modelMapper;
        this.entryService = entryService;
    }

    @GetMapping
    public List<EntryDtoInterface> getAllEventsInDateRange(
            @RequestParam @DateTimeFormat(iso = ISO.DATE) LocalDate from,
            @RequestParam @DateTimeFormat(iso = ISO.DATE) LocalDate to
    ) {
        return entryService.getInDateRange(from, to).stream().map(this::convertToDto).collect(Collectors.toList());
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public EntryDtoInterface createScheduledEntry(@RequestBody @Valid ScheduledEntryDto scheduledEntryDto) {
        ScheduledEntry scheduledEntry = convertToDao(scheduledEntryDto);
        return convertToDto(entryService.create(scheduledEntry));
    }

    @PostMapping("/{scheduledEntryId}/book")
    public EntryDtoInterface bookScheduledEntry(@PathVariable final long scheduledEntryId) throws Exception {
        return convertToDto(entryService.book(scheduledEntryId));
    }

    @PatchMapping({"/{scheduledEntryId}", "/{scheduledEntryId}/{occurrence}"})
    public EntryDtoInterface updateScheduledEntry(
            @PathVariable final Long scheduledEntryId,
            @PathVariable(required = false) final Long occurrence,
            @RequestBody @Valid final ScheduledEntryDto scheduledEntryDto
    ) throws Exception {
        ScheduledEntry scheduledEntry = convertToDao(scheduledEntryDto);
        if (occurrence == null) {
            // modify the scheduled entry for all occurrences
            return convertToDto(entryService.update(scheduledEntryId, scheduledEntry));
        }

        // modify only one scheduled entry
        return convertToDto(entryService.detachAndModifyEntryOccurrence(scheduledEntryId, occurrence, scheduledEntry));
    }

    private EntryDtoInterface convertToDto(final EntryInterface entry) {
        if (entry instanceof Entry) {
            return modelMapper.map(entry, EntryDto.class);
        }
        return modelMapper.map(entry, ScheduledEntryDto.class);
    }

    private EntryDto convertToDto(final Entry entry) {
        return modelMapper.map(entry, EntryDto.class);
    }

    private ScheduledEntry convertToDao(final ScheduledEntryDto scheduledEntryDto) {
        return modelMapper.map(scheduledEntryDto, ScheduledEntry.class);
    }

    private ScheduledEntryDto convertToDto(final ScheduledEntry scheduledEntry) {
        return modelMapper.map(scheduledEntry, ScheduledEntryDto.class);
    }

}
