package ch.kramerit.kassenbuch.dto;

import ch.kramerit.kassenbuch.dto.embeddable.RecurringSetupEmbeddableDto;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.springframework.beans.factory.annotation.Configurable;

import javax.persistence.Embedded;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigDecimal;
import java.time.LocalDate;

@Getter
@Setter
@NoArgsConstructor
@ToString
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
@Configurable
public class ScheduledEntryDto implements EntryDtoInterface, Cloneable {
    protected String type = EntryType.ENTRY_TYPE_SCHEDULED;
    protected Long occurrence = 1L;

    protected Long id;

    @JsonIdentityReference(alwaysAsId = true)
    @NotNull
    protected BankAccountDto bankAccount;

    @NotNull
    @NotBlank
    @Size(max = 255)
    protected String title;

    @NotNull
    protected LocalDate date;

    @NotNull
    protected BigDecimal amount;

    @Embedded
    @Valid
    protected RecurringSetupEmbeddableDto recurringSetup;

    protected ScheduledEntryDto parentEntry;

    @JsonProperty("bank_account")
    public void setBankAccountDto(long id) {
        bankAccount = new BankAccountDto(id);
    }
}
