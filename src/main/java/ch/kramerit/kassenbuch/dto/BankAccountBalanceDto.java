package ch.kramerit.kassenbuch.dto;

import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.math.BigDecimal;

@Getter
@Setter
@NoArgsConstructor
@ToString
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class BankAccountBalanceDto {
    protected String currency;
    @JsonIdentityReference(alwaysAsId = true)
    protected BankAccountDto bankAccount;
    protected BigDecimal amount;
}
