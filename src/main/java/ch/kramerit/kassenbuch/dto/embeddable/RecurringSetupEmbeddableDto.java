package ch.kramerit.kassenbuch.dto.embeddable;

import ch.kramerit.kassenbuch.enumeration.RecurringTypeEnum;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Embeddable;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Embeddable
@Getter
@Setter
@ToString
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class RecurringSetupEmbeddableDto {
    @Enumerated(EnumType.STRING)
    @NotNull
    protected RecurringTypeEnum type;

    @NotNull
    protected Integer separationCount;

    /**
     * Process after holiday TRUE, otherwise FALSE
     */
    @NotNull
    protected Boolean afterHoliday = false;

    protected Integer numberOfOccurrences;

    protected String occurrenceExceptions;

    public List<Long> getOccurrenceExceptions() {
        if (occurrenceExceptions == null || occurrenceExceptions.length() == 0) {
            return List.of();
        }
        return Arrays.stream(occurrenceExceptions.split(",")).map(Long::valueOf).collect(Collectors.toList());
    }

    public void setOccurrenceExceptions(List<Long> exceptions) {
        if (exceptions == null) {
            occurrenceExceptions = null;
            return;
        }
        occurrenceExceptions = exceptions.stream().map(Object::toString).collect(Collectors.joining(","));
    }

    public void addOccurrenceException(Long exception) {
        List<Long> newExceptions = getOccurrenceExceptions();
        if (!newExceptions.contains(exception)) {
            newExceptions.add(exception);
        }
        setOccurrenceExceptions(newExceptions);
    }
}
