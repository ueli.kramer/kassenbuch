package ch.kramerit.kassenbuch.dto;

public abstract class EntryType {
    public static String ENTRY_TYPE_SCHEDULED = "scheduled";
    public static String ENTRY_TYPE_BOOKED = "booked";
}
