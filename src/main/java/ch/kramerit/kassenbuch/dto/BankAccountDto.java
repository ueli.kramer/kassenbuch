package ch.kramerit.kassenbuch.dto;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.*;
import java.time.LocalDate;

@Getter
@Setter
@NoArgsConstructor
@ToString
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class BankAccountDto {
    protected Long id;

    @NotNull
    @Size(min = 3, max = 3)
    protected String currency;

    @NotNull
    @Size(max = 50)
    @NotBlank(message = "Name cannot be empty")
    protected String name;

    @NotNull
    @Size(max = 100)
    @NotBlank(message = "Bank name cannot be empty")
    protected String bank;

    @NotNull
    @Size(max = 50)
    @NotBlank(message = "IBAN cannot be empty")
    protected String iban;

    @NotNull
    protected LocalDate openedDate;

    public BankAccountDto(long id) {
        this.id = id;
    }
}
