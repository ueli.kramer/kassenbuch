package ch.kramerit.kassenbuch.dto;

import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.*;
import java.math.BigDecimal;
import java.time.LocalDate;

@Getter
@Setter
@NoArgsConstructor
@ToString
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class EntryDto implements EntryDtoInterface {
    protected String type = EntryType.ENTRY_TYPE_BOOKED;

    protected Long id;

    @JsonIdentityReference(alwaysAsId = true)
    @NotNull
    protected BankAccountDto bankAccount;

    @NotNull
    @NotBlank
    @Size(max = 255)
    protected String title;

    @NotNull
    protected LocalDate date;

    @NotNull
    protected BigDecimal amount;

    @JsonProperty("bank_account")
    public void setBankAccountDto(long id) {
        bankAccount = new BankAccountDto(id);
    }
}
