package ch.kramerit.kassenbuch.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Setter
@Getter
public class ErrorResponseDto {
    protected String message;
    protected List<String> details;

    public ErrorResponseDto(final String message, final List<String> details) {
        this.message = message;
        this.details = details;
    }
}