package ch.kramerit.kassenbuch.entity.embeddable;

import ch.kramerit.kassenbuch.enumeration.RecurringTypeEnum;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.lang.Nullable;

import javax.persistence.Embeddable;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * DAILY:
 * -
 */
@Embeddable
@Getter
@Setter
@ToString
public class RecurringSetupEmbeddable {
    @Enumerated(EnumType.STRING)
    protected RecurringTypeEnum type;

    protected Integer separationCount;

    /**
     * Process after holiday TRUE, otherwise FALSE
     */
    protected Boolean afterHoliday = false;

    protected Integer numberOfOccurrences;

    protected String occurrenceExceptions;

    public List<Long> getOccurrenceExceptions() {
        if (occurrenceExceptions == null || occurrenceExceptions.length() == 0) {
            return List.of();
        }
        return Arrays.stream(occurrenceExceptions.split(",")).map(Long::valueOf).collect(Collectors.toList());
    }

    public void setOccurrenceExceptions(List<Long> exceptions) {
        if (exceptions == null) {
            occurrenceExceptions = null;
            return;
        }
        occurrenceExceptions = exceptions.stream().map(Object::toString).collect(Collectors.joining(","));
    }

    public void addOccurrenceException(Long exception) {
        List<Long> newExceptions = new ArrayList<>(getOccurrenceExceptions());
        if (!newExceptions.contains(exception)) {
            newExceptions.add(exception);
        }
        setOccurrenceExceptions(newExceptions);
    }
}
