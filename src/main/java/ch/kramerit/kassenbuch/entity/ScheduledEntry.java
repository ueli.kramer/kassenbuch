package ch.kramerit.kassenbuch.entity;

import ch.kramerit.kassenbuch.entity.embeddable.RecurringSetupEmbeddable;
import ch.kramerit.kassenbuch.util.ScheduleUtil;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.beans.factory.annotation.Configurable;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ScheduledFuture;

@Entity(name = "ScheduledEntry")
@Getter
@Setter
@NoArgsConstructor
@ToString
@Configurable
public class ScheduledEntry implements EntryInterface, Cloneable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long id;
    @SuppressWarnings("JpaDataSourceORMInspection")
    @ManyToOne(cascade = {CascadeType.MERGE})
    @JoinColumn(name = "bankAccountId")
    @NotNull
    protected BankAccount bankAccount;
    @NotNull
    protected String title;
    @NotNull
    protected LocalDate date;
    @NotNull
    protected BigDecimal amount;
    @Embedded
    protected RecurringSetupEmbeddable recurringSetup;
    @CreationTimestamp
    @Column(updatable = false)
    protected LocalDateTime createdDateTime;
    @UpdateTimestamp
    protected LocalDateTime updatedDateTime;

    transient protected ScheduledEntry parentEntry;
    transient protected Long occurrence = 1L;

    public Entry toEntry() {
        Entry entry = new Entry();
        entry.setBankAccount(this.getBankAccount());
        entry.setTitle(this.getTitle());
        entry.setAmount(this.getAmount());
        entry.setDate(this.getDate());
        return entry;
    }

    public LocalDate getNextOccurrence(final long occurrence) {
        RecurringSetupEmbeddable recurringSetupEmbeddable = getRecurringSetup();
        if (recurringSetupEmbeddable == null) {
            return null;
        }
        if (recurringSetupEmbeddable.getNumberOfOccurrences() != null && recurringSetupEmbeddable.getNumberOfOccurrences() == 1) {
            return null;
        }
        if (recurringSetupEmbeddable.getOccurrenceExceptions().contains(occurrence)) {
            return null;
        }
        return ScheduleUtil.addInterval(
                this.getDate(),
                recurringSetupEmbeddable.getType(),
                recurringSetupEmbeddable.getSeparationCount(),
                occurrence
        );
    }

    public List<ScheduledEntry> getOccurrencesInDateRange(final LocalDate from, final LocalDate to) {
        if (this.getRecurringSetup() == null) {
            if ((from != null && this.date.isBefore(from)) || this.date.isAfter(to)) {
                return List.of();
            }
            return List.of(this);
        }

        List<ScheduledEntry> scheduledEntries = new ArrayList<>();
        // is recurring
        RecurringSetupEmbeddable recurringSetup = this.getRecurringSetup();
        long occurrence = 0;

        // make steps until we reach "to"
        LocalDate dateCursor;
        while (recurringSetup.getNumberOfOccurrences() == null || recurringSetup.getNumberOfOccurrences() > occurrence) {
            occurrence++;
            dateCursor = ScheduleUtil.addInterval(this.getDate(), recurringSetup.getType(), recurringSetup.getSeparationCount(), occurrence);
            // if the event is not within our range, we can continue searching from there
            if (from != null && dateCursor.isBefore(from)) {
                continue;
            }
            if (dateCursor.isAfter(to)) {
                break;
            }
            // skip occurrence, as it is an exception
            if (recurringSetup.getOccurrenceExceptions().contains(occurrence)) {
                continue;
            }
            scheduledEntries.add(generateScheduledEntry(dateCursor, occurrence));
        }

        // move the entries around to not be on a weekend day or a holiday
        scheduledEntries.forEach(se -> se.setDate(
                ScheduleUtil.getClosestBusinessDay(se.getDate(), recurringSetup.getAfterHoliday())
        ));

        return scheduledEntries;
    }

    public List<ScheduledEntry> getOccurrencesInDateRange(final LocalDate to) {
        return getOccurrencesInDateRange(null, to);
    }

    public ScheduledEntry generateScheduledEntry(final LocalDate date, final Long occurrence) {
        try {
            ScheduledEntry newScheduledEntry = (ScheduledEntry) this.clone();
            newScheduledEntry.setDate(date);
            newScheduledEntry.setParentEntry(this);
            newScheduledEntry.setOccurrence(occurrence);
            return newScheduledEntry;
        } catch (CloneNotSupportedException e) {
            return null;
        }
    }
}
