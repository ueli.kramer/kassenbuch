package ch.kramerit.kassenbuch.entity;

import java.math.BigDecimal;

public interface EntryInterface {
    BigDecimal getAmount();
}
