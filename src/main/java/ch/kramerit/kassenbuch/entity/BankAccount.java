package ch.kramerit.kassenbuch.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity(name = "BankAccount")
@Getter
@Setter
@NoArgsConstructor
@ToString
public class BankAccount {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long id;

    @NotNull
    protected String currency;

    @NotNull
    protected String name;

    @NotNull
    protected String bank;

    @NotNull
    protected String iban;

    @NotNull
    protected LocalDate openedDate;

    @CreationTimestamp
    protected LocalDateTime createdDateTime;

    @UpdateTimestamp
    protected LocalDateTime updatedDateTime;
}
