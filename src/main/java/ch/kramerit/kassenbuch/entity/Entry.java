package ch.kramerit.kassenbuch.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity(name = "Entry")
@Getter
@Setter
@NoArgsConstructor
@ToString
public class Entry implements EntryInterface {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long id;

    @SuppressWarnings("JpaDataSourceORMInspection")
    @ManyToOne(cascade = {CascadeType.MERGE})
    @JoinColumn(name = "bankAccountId")
    @NotNull
    protected BankAccount bankAccount;

    @NotNull
    protected String title;

    @NotNull
    protected LocalDate date;

    @NotNull
    protected BigDecimal amount;

    @CreationTimestamp
    protected LocalDateTime createdDateTime;

    @UpdateTimestamp
    protected LocalDateTime updatedDateTime;
}
