package ch.kramerit.kassenbuch.enumeration;

public enum RecurringTypeEnum {
    DAILY,
    WEEKLY,
    MONTHLY,
    YEARLY
}
