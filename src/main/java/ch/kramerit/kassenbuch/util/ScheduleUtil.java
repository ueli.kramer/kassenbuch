package ch.kramerit.kassenbuch.util;

import ch.kramerit.kassenbuch.enumeration.RecurringTypeEnum;
import de.jollyday.HolidayManager;
import org.springframework.stereotype.Component;

import java.sql.Date;
import java.time.LocalDate;
import java.util.Calendar;
import java.util.GregorianCalendar;

@Component
public class ScheduleUtil {
    private static HolidayManager holidayManager;
    private static Calendar calendar;

    public ScheduleUtil(HolidayManager holidayManager) {
        ScheduleUtil.calendar = GregorianCalendar.getInstance();
        ScheduleUtil.holidayManager = holidayManager;
    }

    public static boolean isWeekday(final LocalDate date) {
        calendar.setTime(Date.valueOf(date));
        int day = calendar.get(Calendar.DAY_OF_WEEK);
        return day != Calendar.SUNDAY && day != Calendar.SATURDAY;
    }

    public static boolean isHoliday(final LocalDate date) {
        return holidayManager.isHoliday(date, "be");
    }

    public static LocalDate getClosestBusinessDay(LocalDate date, final boolean afterHoliday) {
        while (isHoliday(date) || !isWeekday(date)) {
            if (afterHoliday) {
                date = date.plusDays(1L);
            } else {
                date = date.minusDays(1L);
            }
        }
        return date;
    }

    public static LocalDate addInterval(final LocalDate date, final RecurringTypeEnum recurringTypeEnum, final long separationCount, long occurrence) {
        occurrence--;
        switch (recurringTypeEnum) {
            case DAILY:
                return date.plusDays(separationCount * occurrence);
            case WEEKLY:
                return date.plusWeeks(separationCount * occurrence);
            case MONTHLY:
                return date.plusMonths(separationCount * occurrence);
        }
        return date.plusYears(separationCount * occurrence);
    }
}
