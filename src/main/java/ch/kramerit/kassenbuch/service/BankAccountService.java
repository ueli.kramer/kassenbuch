package ch.kramerit.kassenbuch.service;

import ch.kramerit.kassenbuch.entity.BankAccount;
import ch.kramerit.kassenbuch.exception.BankAccountNotFoundException;
import ch.kramerit.kassenbuch.repository.BankAccountRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BankAccountService {
    private final BankAccountRepository bankAccountRepository;

    public BankAccountService(final BankAccountRepository bankAccountRepository) {
        this.bankAccountRepository = bankAccountRepository;
    }

    public List<BankAccount> getAll() {
        return bankAccountRepository.findAll();
    }

    public BankAccount get(final long bankAccountId) {
        return bankAccountRepository.findById(bankAccountId).orElseThrow(BankAccountNotFoundException::new);
    }

    public BankAccount create(final BankAccount bankAccount) {
        return bankAccountRepository.save(bankAccount);
    }

    public BankAccount update(final long id, final BankAccount bankAccount) {
        BankAccount bankAccountOnFile = bankAccountRepository.findById(id).orElseThrow(BankAccountNotFoundException::new);

        bankAccountOnFile.setName(bankAccount.getName());
        bankAccountOnFile.setBank(bankAccount.getBank());
        bankAccountOnFile.setCurrency(bankAccount.getCurrency());
        bankAccountOnFile.setIban(bankAccount.getIban());
        bankAccountOnFile.setOpenedDate(bankAccount.getOpenedDate());
        return bankAccountRepository.save(bankAccountOnFile);
    }
}
