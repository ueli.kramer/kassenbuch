package ch.kramerit.kassenbuch.service;

import ch.kramerit.kassenbuch.entity.BankAccount;
import ch.kramerit.kassenbuch.entity.Entry;
import ch.kramerit.kassenbuch.entity.EntryInterface;
import ch.kramerit.kassenbuch.entity.ScheduledEntry;
import ch.kramerit.kassenbuch.entity.embeddable.RecurringSetupEmbeddable;
import ch.kramerit.kassenbuch.exception.BankAccountNotFoundException;
import ch.kramerit.kassenbuch.exception.InvalidBankAccountException;
import ch.kramerit.kassenbuch.exception.ScheduledEntryNotFoundException;
import ch.kramerit.kassenbuch.repository.BankAccountRepository;
import ch.kramerit.kassenbuch.repository.EntryRepository;
import ch.kramerit.kassenbuch.repository.ScheduledEntryRepository;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class EntryService {
    private final BankAccountRepository bankAccountRepository;
    private final ScheduledEntryRepository scheduledEntryRepository;
    private final EntryRepository entryRepository;

    public EntryService(final BankAccountRepository bankAccountRepository,
                        final ScheduledEntryRepository scheduledEntryRepository,
                        final EntryRepository entryRepository) {
        this.bankAccountRepository = bankAccountRepository;
        this.scheduledEntryRepository = scheduledEntryRepository;
        this.entryRepository = entryRepository;
    }

    public Entry create(final Entry entry) {
        bankAccountRepository.findById(entry.getBankAccount().getId()).orElseThrow(InvalidBankAccountException::new);
        return entryRepository.save(entry);
    }

    public ScheduledEntry create(final ScheduledEntry scheduledEntry) {
        bankAccountRepository.findById(scheduledEntry.getBankAccount().getId()).orElseThrow(InvalidBankAccountException::new);
        return scheduledEntryRepository.save(scheduledEntry);
    }

    public Entry book(final long scheduledEntryId) throws Exception {
        ScheduledEntry scheduledEntry = scheduledEntryRepository.findById(scheduledEntryId).orElseThrow(ScheduledEntryNotFoundException::new);

        // create new booked entry
        Entry entry = scheduledEntry.toEntry();
        entry = entryRepository.saveAndFlush(entry);

        // there is a next occurrence, we nee dto decrease the number of occurrence and update the date
        RecurringSetupEmbeddable recurringSetup = scheduledEntry.getRecurringSetup();
        if (recurringSetup == null) {
            // is no recurring
            scheduledEntryRepository.delete(scheduledEntry);
            return entry;
        }
        recurringSetup.setOccurrenceExceptions(
                recurringSetup.getOccurrenceExceptions().stream()
                        .map(e -> e - 1) // decrease all the values
                        .filter(e -> e > 0) // filter all negative values
                        .distinct()
                        .collect(Collectors.toList()) // to list
        );
        int i = 1;
        while (recurringSetup.getNumberOfOccurrences() == null || i < recurringSetup.getNumberOfOccurrences()) {
            if (scheduledEntry.getNextOccurrence(i) != null) {
                scheduledEntry.setDate(scheduledEntry.getNextOccurrence(i));
                break;
            }
            i++;
        }
        if (recurringSetup.getNumberOfOccurrences() != null) {
            recurringSetup.setNumberOfOccurrences(recurringSetup.getNumberOfOccurrences() - i);
            if (recurringSetup.getNumberOfOccurrences() < 1) {
                scheduledEntryRepository.delete(scheduledEntry);
                return entry;
            }
        }
        scheduledEntryRepository.saveAndFlush(scheduledEntry);
        return entry;
    }

    public ScheduledEntry update(final long id, final ScheduledEntry scheduledEntry) {
        ScheduledEntry scheduledEntryOnFile = scheduledEntryRepository.findById(id).orElseThrow(ScheduledEntryNotFoundException::new);

        scheduledEntryOnFile.setTitle(scheduledEntry.getTitle());

        BankAccount bankAccount = bankAccountRepository.findById(scheduledEntry.getBankAccount().getId()).orElseThrow(BankAccountNotFoundException::new);
        scheduledEntryOnFile.setBankAccount(bankAccount);

        scheduledEntryOnFile.setAmount(scheduledEntry.getAmount());
        scheduledEntryOnFile.setDate(scheduledEntry.getDate());

        RecurringSetupEmbeddable recurringSetup = scheduledEntry.getRecurringSetup();
        if (recurringSetup == null) {
            scheduledEntryOnFile.setRecurringSetup(null);
            return scheduledEntryRepository.save(scheduledEntryOnFile);
        }

        RecurringSetupEmbeddable recurringSetupOnFile = new RecurringSetupEmbeddable();
        recurringSetupOnFile.setType(recurringSetup.getType());
        recurringSetupOnFile.setAfterHoliday(recurringSetup.getAfterHoliday());
        recurringSetupOnFile.setSeparationCount(recurringSetup.getSeparationCount());
        recurringSetupOnFile.setNumberOfOccurrences(recurringSetup.getNumberOfOccurrences());
        scheduledEntryOnFile.setRecurringSetup(recurringSetupOnFile);
        return scheduledEntryRepository.save(scheduledEntryOnFile);
    }

    public ScheduledEntry detachAndModifyEntryOccurrence(
            final long id,
            final long occurrence,
            final ScheduledEntry scheduledEntry
    ) throws Exception {
        ScheduledEntry scheduledEntryOnFile = scheduledEntryRepository.findById(id).orElseThrow(ScheduledEntryNotFoundException::new);

        if (scheduledEntryOnFile.getRecurringSetup() == null) {
            throw new Exception("Is not recurring entry");
        }

        if (scheduledEntryOnFile.getRecurringSetup().getNumberOfOccurrences() != null && scheduledEntryOnFile.getRecurringSetup().getNumberOfOccurrences() < occurrence) {
            throw new Exception("Invalid number of occurrence");
        }

        // add the occurrence to exception
        scheduledEntryOnFile.getRecurringSetup().addOccurrenceException(occurrence);

        // detached entry should not be recurring setup
        // @todo: find cleaner way to do this
        scheduledEntry.setId(null);
        scheduledEntry.setRecurringSetup(null);
        scheduledEntry.setParentEntry(null);
        long bankAccountId = scheduledEntry.getBankAccount().getId();
        BankAccount bankAccount = bankAccountRepository.findById(bankAccountId).orElseThrow(BankAccountNotFoundException::new);
        scheduledEntry.setBankAccount(bankAccount);
        scheduledEntry.setCreatedDateTime(LocalDateTime.now());

        scheduledEntryRepository.save(scheduledEntry);
        scheduledEntryRepository.save(scheduledEntryOnFile);

        return scheduledEntry;
    }

    public List<Entry> getBookedEntriesByBankAccountAndDate(final BankAccount bankAccount, final LocalDate date) {
        return entryRepository.findAllByBankAccount_IdAndDateLessThanEqual(bankAccount.getId(), date);
    }

    public List<EntryInterface> getInDateRange(final LocalDate from, final LocalDate to) {
        List<Entry> entries = entryRepository.findAllByFromAndToDate(from, to);

        // get scheduled entries in date range
        List<ScheduledEntry> scheduledEntries = scheduledEntryRepository.findAllByFromAndToDate(from, to);
        scheduledEntries = scheduledEntries.stream().flatMap(se -> se.getOccurrencesInDateRange(from, to).stream()).collect(Collectors.toList());

        return Stream.concat(entries.stream(), scheduledEntries.stream()).collect(Collectors.toList());
    }

    public List<EntryInterface> getAllEntriesByBankAccountAndDate(final BankAccount bankAccount, final LocalDate to) {
        List<Entry> entries = entryRepository.findAllByBankAccountAndToDate(bankAccount, to);

        // get scheduled entries in date range
        List<ScheduledEntry> scheduledEntries = scheduledEntryRepository.findAllByBankAccountAndToDate(bankAccount, to);
        scheduledEntries = scheduledEntries.stream().flatMap(se -> se.getOccurrencesInDateRange(to).stream()).collect(Collectors.toList());

        return Stream.concat(entries.stream(), scheduledEntries.stream()).collect(Collectors.toList());
    }
}
