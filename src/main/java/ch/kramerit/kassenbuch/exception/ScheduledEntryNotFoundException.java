package ch.kramerit.kassenbuch.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class ScheduledEntryNotFoundException extends RuntimeException {
}
