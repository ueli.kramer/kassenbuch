CREATE TABLE bank_account
(
    id                bigint(20) NOT NULL AUTO_INCREMENT,
    currency          varchar(3)   NOT NULL,
    name              varchar(50)  NOT NULL,
    bank              varchar(100) NOT NULL,
    iban              varchar(50)  NOT NULL,
    opened_date       date         NOT NULL,
    created_date_time date         NOT NULL,
    updated_date_time date         NOT NULL,
    PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE entry
(
    id                bigint(20) NOT NULL AUTO_INCREMENT,
    bank_account_id   bigint(20) NOT NULL,
    date              date          NOT NULL,
    title             varchar(255)  NOT NULL,
    amount            decimal(6, 2) NOT NULL,
    created_date_time date          NOT NULL,
    updated_date_time date          NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (bank_account_id) REFERENCES bank_account (id) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE scheduled_entry
(
    id                                    bigint(20) NOT NULL AUTO_INCREMENT,
    bank_account_id                       bigint(20) NOT NULL,
    date                                  date          NOT NULL,
    title                                 varchar(255)  NOT NULL,
    amount                                decimal(6, 2) NOT NULL,
    recurring_setup_type                  varchar(7),
    recurring_setup_separation_count      int(4),
    recurring_setup_after_holiday         boolean,
    recurring_setup_number_of_occurrences int(3),
    recurring_setup_occurrence_exceptions varchar(255),
    created_date_time                     date          NOT NULL,
    updated_date_time                     date          NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (bank_account_id) REFERENCES bank_account (id) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
